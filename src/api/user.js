import { axios } from '@/utils/request'

// 新增用户
export function addUser(data) {
  return axios({
    url: '/sys/user',
    method: 'post',
    data: data
  })
}

// 获取用户列表
export function queryUserList(parms) {
  return axios({
    url: '/sys/user',
    method: 'get',
    params: parms
  })
}

// 编辑用户
export function updateUser(data) {
  return axios({
    url: '/sys/user',
    method: 'put',
    data: data
  })
}

// 重置密码
export function restPass(data) {
  return axios({
    url: '/sys/user/restPass',
    method: 'put',
    data: data
  })
}

// 删除用户
export function deleteUser(id) {
  return axios({
    url: '/sys/user/' + id,
    method: 'delete'
  })
}

// 获取用户个人信息
export function getUserInfo() {
  return axios({
    url: '/sys/user/info',
    method: 'get'
  })
}

// 重复校验
export function repeatCheck(parms) {
  return axios({
    url: '/sys/repeatCheck/check',
    headers: {
      'Authorization': 'Basic cGVyeDoxMjM0NTY='
    },
    method: 'get',
    params: parms
  })
}

// 修改密码
export function updatePass(parms) {
  return axios({
    url: '/sys/user/updatePass',
    method: 'put',
    data: parms
  })
}

// 修改邮箱
export function updateEmail(parms) {
  return axios({
    url: '/user/updateEmail',
    method: 'put',
    params: parms
  })
}

// 发送邮箱验证码
export function resetEmail(parms) {
  return axios({
    url: '/user/sendMailCode',
    method: 'post',
    params: parms
  })
}

// 发送短信验证码
export function sendSms(phone) {
  return request({
    url: '/sendCode/' + phone,
    method: 'post'
  })
}

